#include "UnityPluginInterface.h"

#if UNITY_WIN
#   include <Windows.h>
#	undef min
#	undef max
#	if SUPPORT_D3D9
#		include <d3d9.h>
#		if 1
#			define STATIC_POOL D3DPOOL_DEFAULT
#		else
#			define STATIC_POOL D3DPOOL_MANAGED
#		endif
#	endif
#	if SUPPORT_D3D11
#		include <d3d11.h>
#	endif
#	include <cassert>
	static inline void AssertHR(HRESULT hr)
	{
		assert(SUCCEEDED(hr));
	}
# else
#   include <stdio.h>
#endif

#include <algorithm>
#include <new>
#include <math.h>
#include <string.h>

#if SUPPORT_OPENGL
#	include <gl/glew.h>
#elif SUPPORT_OPENGLES
#	if UNITY_IPHONE
#		include <OpenGLES/ES2/gl.h>
#	elif UNITY_ANDROID
#		include <GLES2/gl2.h>
#	endif
#endif

static void DebugLog(const char *str)
{
#if UNITY_WIN
	OutputDebugStringA(str);
#else
	printf("%s", str);
#endif
}

struct Vertex
{
	float x, y, z, s, t;
};

#if UNITY_WIN
#   pragma pack(push, 1)
#endif
struct Shroom
{
	float x, y, z,
		scaleX, scaleY, angle,
		r, g, b, a,
		s, t, w, h;
}
#ifndef UNITY_WIN
__attribute__((packed))
#endif
;
#if UNITY_WIN
#   pragma pack(push, 1)
#endif

//#define DO_CULL_FACE

#define ALPHA_TEST_TRESHOLD_F 0.25f
#define ALPHA_TEST_TRESHOLD_STR "0.25"

#ifdef SUPPORT_OPENGLES

#   define SHADER_ATTRIB_POS 1
#   define SHADER_ATTRIB_UV 2

#   define VSHADER_SRC \
	"attribute highp vec3 pos;\n"\
	"attribute highp vec2 uv;\n"\
	"varying highp vec2 st;"\
	"uniform highp mat4 worldMatrix;\n"\
	"uniform highp mat4 projMatrix;\n"\
	"void main()\n"\
	"{\n"\
	"	st = uv;\n"\
	"	gl_Position = (projMatrix * worldMatrix) * vec4(pos, 1);\n"\
	"}\n"

#   define FSHADER_NORMAL_SRC \
	"varying highp vec2 st;"\
	"uniform sampler2D texSampler;"\
	"void main()\n"\
	"{\n"\
	"	gl_FragColor = texture2D(texSampler, st);\n"\
	"}\n"

#   define FSHADER_DISCARD_SRC(treshold)\
	"varying highp vec2 st;"\
	"uniform sampler2D texSampler;"\
	"void main()\n"\
	"{\n"\
	"   lowp vec4 col = texture2D(texSampler, st);\n"\
	"   if (col.a < " treshold ")\n"\
	"       discard;\n"\
	"   else\n"\
	"       gl_FragColor = texture2D(texSampler, st);\n"\
	"}\n"

static GLuint g_CurProgram = 0, g_ProgNormal, g_ProgDiscard;
static GLint g_TextureUIdx[2], g_WorldMatrixUIdx[2], g_ProjMatrixUIdx[2];

static GLuint CreateShader(GLenum type, const char *text)
{
	GLuint ret = glCreateShader(type);
	glShaderSource(ret, 1, &text, NULL);
	glCompileShader(ret);

#   ifdef DEBUG
	int logLen;
	glGetShaderiv(ret, GL_INFO_LOG_LENGTH, &logLen);

	DebugLog("Begin of shader compilation output:\n");
	if (logLen > 0)
	{
		GLchar *log = new GLchar[logLen];
		glGetShaderInfoLog(ret, logLen, &logLen, log);
		DebugLog(log);
		delete[] log;
	}
	else
		DebugLog("Compiled Succesfully");
	DebugLog("\nEnd\n");
#   endif

	return ret;
}

static GLuint CreateProgram(GLuint vShader, GLuint pShadert)
{
	GLuint ret = glCreateProgram();

	glBindAttribLocation(ret, SHADER_ATTRIB_POS, "pos");
	glBindAttribLocation(ret, SHADER_ATTRIB_UV, "uv");

	glAttachShader(ret, vShader);
	glAttachShader(ret, pShadert);

	glLinkProgram(ret);

#   ifdef DEBUG
	int logLen;
	glGetProgramiv(ret, GL_INFO_LOG_LENGTH, &logLen);

	DebugLog("Begin of program compilation output:\n");
	if (logLen > 0)
	{
		GLchar *log = new GLchar[logLen];
		glGetProgramInfoLog(ret, logLen, &logLen, log);
		DebugLog(log);
		delete[] log;
	}
	else
		DebugLog("Compiled & Linked Succesfully");
	DebugLog("\nEnd\n");
#   endif

	return ret;
}

#endif

static Vertex *g_Data;
static int g_DeviceType = -1;
static enum RENDER_MODE
{
	RE_DIRECT = 0,
	RE_DYNAMIC,
	RE_STATIC,
} g_Mode;
static unsigned g_Capacity = -1;

#ifdef SUPPORT_D3D9
namespace DX
{
	namespace
	{
		static union
		{
			IDirect3DDevice9	*D3D9Device;
			ID3D11Device		*D3D11Device;
		};
		unsigned int segment[2];
		static union
		{
			IDirect3DVertexBuffer9	*D3D9_VB;
			ID3D11Buffer			*D3D11_VB;
		};

		inline unsigned int Size()
		{
			const unsigned int size = g_Capacity * 6 * sizeof(Vertex);
			switch (g_Mode)
			{
			case RE_DIRECT:
			case RE_DYNAMIC:
				return std::max(size, 1u * 1024u * 1024u);
			case RE_STATIC:
				return size;
			default:
				assert(false);
				__assume(false);
			}
		}

		void CreateDX9()
		{
			DWORD usage = D3DUSAGE_WRITEONLY;
			D3DPOOL pool;
			switch (g_Mode)
			{
			case RE_DIRECT:
				return;
			case RE_DYNAMIC:
				usage |= D3DUSAGE_DYNAMIC;
				pool = D3DPOOL_DEFAULT;
				break;
			case RE_STATIC:
				pool = STATIC_POOL;
				break;
			default:
				assert(false);
				__assume(false);
			}
			AssertHR(D3D9Device->CreateVertexBuffer(Size(), usage, D3DFVF_XYZ | D3DFVF_TEX1, pool, &D3D9_VB, NULL));
		}

		void FreeDX9(bool force)
		{
			if (D3D9_VB && (force || g_Mode == D3DUSAGE_DYNAMIC || STATIC_POOL == D3DPOOL_DEFAULT))
			{
				D3D9_VB->Release();
				D3D9_VB = nullptr;
			}
		}

		void CreateDX11()
		{
		}

		void FreeDX11()
		{
			if (D3D11_VB)
			{
				D3D11_VB->Release();
				D3D11_VB = nullptr;
			}
		}
	}
}
#endif

static GLuint g_Vbo = -1;
static void CreateVBO()
{
	if (g_Mode == RE_DIRECT) return;

	switch (g_DeviceType)
	{
	case kGfxRendererD3D9:
	case kGfxRendererD3D11:
		return;
	}

	const GLuint size = g_Capacity * 6 * sizeof(Vertex);
	const bool dynamic = g_Mode == RE_DYNAMIC;
#ifdef SUPPORT_OPENGL
	if (!GLEW_ARB_vertex_buffer_object)
		return;
#endif

	if (g_Vbo == -1)
	{
		glGenBuffers(1, &g_Vbo);
		glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
		glBufferData(GL_ARRAY_BUFFER, size, g_Data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, size, g_Data);
	}
}

static void FreeVBO()
{
	if (g_Vbo == -1)
		return;

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &g_Vbo);

	g_Vbo = -1;
}

extern "C" bool EXPORT_API ShroomzInitialize(unsigned capacity, RENDER_MODE mode)
{
	if (g_Capacity != -1)
		return false;

	g_Capacity = capacity;
	g_Mode = mode;

	bool need_RAM_alloc = true;

	switch (g_DeviceType)
	{
	case kGfxRendererD3D9:
		DX::CreateDX9();
		need_RAM_alloc = !DX::D3D9_VB;
		break;
	case kGfxRendererD3D11:
		DX::CreateDX11();
		need_RAM_alloc = false;
		break;
	}

	if (need_RAM_alloc)
	{
		try
		{
			g_Data = new Vertex[g_Capacity * 6];
		}
		catch (std::bad_alloc &error)
		{
			DebugLog(error.what());
			return false;
		}
	}

	CreateVBO();

	return true;
}

static void *g_TexturePointer;
extern "C" void EXPORT_API ShroomzSetTexture(void *texturePtr)
{
	g_TexturePointer = texturePtr;
}

static inline void quickSort(Shroom *shroomz, int first, int last)
{
	int pivot = first;

	if (first < last)
	{
		Shroom pivotElement = shroomz[first];

		for (int i = first + 1; i <= last; ++i)
			if (shroomz[i].z > pivotElement.z) // If you want to sort array in the other order, change ">" to "<=".
			{
				++pivot;
				Shroom t = shroomz[i];
				shroomz[i] = shroomz[pivot];
				shroomz[pivot] = t;
			}

		Shroom t = shroomz[pivot];
		shroomz[pivot] = shroomz[first];
		shroomz[first] = t;

		quickSort(shroomz, first, pivot - 1);
		quickSort(shroomz, pivot + 1, last);
	}
}

extern "C" void EXPORT_API ShroomzUpdate(Shroom *shroomz, unsigned count, bool doSort)
{
	if (!shroomz || count == 0 || count == -1)
		return;

	if (doSort)
		quickSort(shroomz, 0, count - 1);

	volatile Vertex *const dst = g_DeviceType == kGfxRendererD3D9 && g_Mode != RE_DIRECT ? [count]()
	{
		using namespace DX;
		const auto size = count * 6 * sizeof(Vertex);
		const bool overflow = Size() - segment[1] >= size;
		DWORD lock_flags = g_Mode == RE_DYNAMIC ? overflow ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD : 0;
		if (overflow)
			segment[1] = 0;
		Vertex *locked;
		AssertHR(D3D9_VB->Lock(segment[1], size, (void **)&locked, lock_flags));
		segment[0] = segment[1];
		segment[1] += size;
		return locked;
	}() : g_Data;

	for (unsigned i = 0; i < count; ++i)
	{
		const unsigned idx = i * 6;

		const float cos = cosf(shroomz[i].angle), sin = sinf(shroomz[i].angle),
			&x = shroomz[i].x, oxc = 0.5f * shroomz[i].scaleX * cos, oxs = 0.5f * shroomz[i].scaleX * sin,
			&y = shroomz[i].y, oyc = 0.5f * shroomz[i].scaleY * cos, oys = 0.5f * shroomz[i].scaleY * sin;

		dst[idx + 0].x = (-oxc + oys) + x;
		dst[idx + 1].x = (-oxc - oys) + x;
		dst[idx + 2].x = (+oxc + oys) + x;
		dst[idx + 3].x = (-oxc - oys) + x;
		dst[idx + 4].x = (+oxc - oys) + x;
		dst[idx + 5].x = (+oxc + oys) + x;

		dst[idx + 0].y = (-oxs - oyc) + y;
		dst[idx + 1].y = (-oxs + oyc) + y;
		dst[idx + 2].y = (+oxs - oyc) + y;
		dst[idx + 3].y = (-oxs + oyc) + y;
		dst[idx + 4].y = (+oxs + oyc) + y;
		dst[idx + 5].y = (+oxs - oyc) + y;

		const float &z = shroomz[i].z;
		dst[idx + 0].z = z;
		dst[idx + 1].z = z;
		dst[idx + 2].z = z;
		dst[idx + 3].z = z;
		dst[idx + 4].z = z;
		dst[idx + 5].z = z;

		const float &s = shroomz[i].s, t = 1.f - shroomz[i].t,
			w = s + shroomz[i].w, h = t - shroomz[i].h;

		dst[idx + 0].s = s;
		dst[idx + 1].s = s;
		dst[idx + 2].s = w;
		dst[idx + 3].s = s;
		dst[idx + 4].s = w;
		dst[idx + 5].s = w;

		dst[idx + 0].t = h;
		dst[idx + 1].t = t;
		dst[idx + 2].t = h;
		dst[idx + 3].t = t;
		dst[idx + 4].t = t;
		dst[idx + 5].t = h;
	}

	if (g_DeviceType == kGfxRendererD3D9 && g_Mode != RE_DIRECT)
		AssertHR(DX::D3D9_VB->Unlock());

	CreateVBO();
}

static bool g_DoBlending = false;
extern "C" void EXPORT_API ShroomzToggleBlending(bool doBlending)
{
	g_DoBlending = doBlending;
#ifdef SUPPORT_OPENGLES
	if (g_DoBlending)
		g_CurProgram = g_ProgNormal;
	else
		g_CurProgram = g_ProgDiscard;
#endif
}

static float g_WorldMatrix[16], g_ProjectionMatrix[16];
extern "C" void EXPORT_API ShroomzSetMatrices(const float *worldMatrix, const float *projectionMatrix)
{
	memcpy(g_WorldMatrix, worldMatrix, 16 * sizeof(float));
	memcpy(g_ProjectionMatrix, projectionMatrix, 16 * sizeof(float));
	switch (g_DeviceType)
	{
	case kGfxRendererD3D9:
	case kGfxRendererD3D11:
		{
			auto &proj = reinterpret_cast<float (&)[4][4]>(g_ProjectionMatrix);
			for (int i = 0; i < 4; i++)
				proj[i][2] = (proj[i][2] + proj[i][3]) * .5f;
	}
		break;
	}
}

extern "C" void EXPORT_API ShroomzFree()
{
	if (g_Capacity == -1)
		return;

#ifdef SUPPORT_OPENGLES
	glUseProgram(0);
	glDeleteProgram(g_ProgNormal);
	glDeleteProgram(g_ProgDiscard);
#endif

	if (g_Mode != RE_DIRECT)
		FreeVBO();

	switch (g_DeviceType)
	{
	case kGfxRendererD3D9:
		DX::FreeDX9(true);
		break;
	case kGfxRendererD3D11:
		DX::FreeDX11();
		break;
	}

	delete[] g_Data;

	g_TexturePointer = NULL;
	g_DoBlending = false;
	g_Capacity = -1;
	g_Mode = RE_DIRECT;
}

// --------------------------------------------------------------------------
// UnitySetGraphicsDevice
extern "C" void EXPORT_API UnitySetGraphicsDevice(void *device, int deviceType, int eventType)
{
	g_DeviceType = deviceType;

	switch (deviceType)
	{
#if SUPPORT_D3D9
	case kGfxRendererD3D9:
	{
		DebugLog("Set D3D9 graphics device\n");

		DX::D3D9Device = (IDirect3DDevice9 *)device;

		switch (eventType)
		{
		case kGfxDeviceEventInitialize:
		case kGfxDeviceEventAfterReset:
			if (!DX::D3D9_VB)
				DX::CreateDX9();
			break;
		case kGfxDeviceEventBeforeReset:
		case kGfxDeviceEventShutdown:
			DX::FreeDX9(false);
			break;
		}
	}
	break;
#endif

#if SUPPORT_OPENGL
	case kGfxRendererOpenGL:
	{
		DebugLog("Set OpenGL graphics device\n");

		GLenum glew_res = glewInit();

		if (glew_res != GLEW_OK)
			DebugLog("Error initializing GLEW\n");
	}
	break;
#endif

#ifdef SUPPORT_OPENGLES
	case kGfxRendererOpenGLES20Mobile:
	{
		DebugLog("Set OpenGLES 2.0 device\n");

		GLuint vShader, fShaderNormal, fShaderDiscard;
		vShader = CreateShader(GL_VERTEX_SHADER, VSHADER_SRC);
		fShaderNormal = CreateShader(GL_FRAGMENT_SHADER, FSHADER_NORMAL_SRC);
		fShaderDiscard = CreateShader(GL_FRAGMENT_SHADER, FSHADER_DISCARD_SRC(ALPHA_TEST_TRESHOLD_STR));

#       undef VSHADER_SRC
#       undef FSHADER_NORMAL_SRC
#       undef FSHADER_DISCARD_SRC

		g_ProgNormal = CreateProgram(vShader, fShaderNormal);
		g_ProgDiscard = CreateProgram(vShader, fShaderDiscard);

		glDeleteShader(vShader);
		glDeleteShader(fShaderNormal);
		glDeleteShader(fShaderDiscard);

		// Uniforms will be identical for both programs (on real device) but let's do it safe way.
		g_TextureUIdx[0] = glGetUniformLocation(g_ProgNormal, "texSampler");
		g_WorldMatrixUIdx[0] = glGetUniformLocation(g_ProgNormal, "worldMatrix");
		g_ProjMatrixUIdx[0] = glGetUniformLocation(g_ProgNormal, "projMatrix");

		g_TextureUIdx[1] = glGetUniformLocation(g_ProgDiscard, "texSampler");
		g_WorldMatrixUIdx[1] = glGetUniformLocation(g_ProgDiscard, "worldMatrix");
		g_ProjMatrixUIdx[1] = glGetUniformLocation(g_ProgDiscard, "projMatrix");

		g_CurProgram = g_ProgDiscard;
	}
		else
			DebugLog("Wrong OpenGLES device\n");
			break;
#endif
	}
}

// --------------------------------------------------------------------------
// UnityRenderEvent
// This will be called for GL.IssuePluginEvent script calls; eventID will
// be the integer passed to IssuePluginEvent.
extern "C" void EXPORT_API UnityRenderEvent(int eventID)
{
	const unsigned short first = (eventID >> 16) & 0xFFFF, count = eventID & 0xFFFF;

	switch (g_DeviceType)
	{
#if SUPPORT_D3D9
	case kGfxRendererD3D9:
	{
#ifdef DO_CULL_FACE
#	define D3D9_CULL D3DCULL_CCW
#else
#	define D3D9_CULL D3DCULL_NONE
#endif
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_CULLMODE, D3D9_CULL));
#undef D3D9_CULL

		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_LIGHTING, FALSE));
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL));
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE));

		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE));
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER));
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ALPHAREF, g_DoBlending ? 0 : ALPHA_TEST_TRESHOLD_F * 255));
		AssertHR(DX::D3D9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, g_DoBlending ? TRUE : FALSE));
		if (g_DoBlending)
		{
			AssertHR(DX::D3D9Device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE));
			AssertHR(DX::D3D9Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD));
			AssertHR(DX::D3D9Device->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_SRCALPHA));
			AssertHR(DX::D3D9Device->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_INVSRCALPHA));
		}

		const float identityMatrix[16] = { 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f };

		AssertHR(DX::D3D9Device->SetTransform(D3DTS_WORLD, (const D3DMATRIX*)g_WorldMatrix));
		AssertHR(DX::D3D9Device->SetTransform(D3DTS_VIEW, (const D3DMATRIX*)identityMatrix));
		AssertHR(DX::D3D9Device->SetTransform(D3DTS_PROJECTION, (const D3DMATRIX*)g_ProjectionMatrix));

		AssertHR(DX::D3D9Device->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1));

		AssertHR(DX::D3D9Device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1));
		AssertHR(DX::D3D9Device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE));
		AssertHR(DX::D3D9Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1));
		AssertHR(DX::D3D9Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE));
		AssertHR(DX::D3D9Device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE));
		AssertHR(DX::D3D9Device->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE));

		AssertHR(DX::D3D9Device->SetTexture(0, (IDirect3DTexture9 *)g_TexturePointer));

		switch (g_Mode)
		{
		case RE_DIRECT:
			AssertHR(DX::D3D9Device->DrawPrimitiveUP(D3DPT_TRIANGLELIST, count * 2, g_Data + first * 6 * sizeof(Vertex), sizeof(Vertex)));
			break;
		case RE_DYNAMIC:
		case RE_STATIC:
			AssertHR(DX::D3D9Device->SetStreamSource(0, DX::D3D9_VB, DX::segment[0], sizeof(Vertex)));
			AssertHR(DX::D3D9Device->DrawPrimitive(D3DPT_TRIANGLELIST, first * 6, count * 2));
			break;
		default:
			assert(false);
			__assume(false);
		}
	}
	break;
#endif

#if SUPPORT_OPENGL || SUPPORT_OPENGLES
	case kGfxRendererOpenGL:
	case kGfxRendererOpenGLES20Mobile:
	{
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_FALSE);
		glEnable(GL_DEPTH_TEST);

#ifdef DO_CULL_FACE
		glCullFace(GL_BACK);
		glFrontFace(GL_CW);
		glEnable(GL_CULL_FACE);
#else
		glDisable(GL_CULL_FACE);
#endif

		if (g_DoBlending)
		{
#   ifdef SUPPORT_OPENGL
			glDisable(GL_ALPHA_TEST);
#   endif
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else
		{
			glDisable(GL_BLEND);
#   ifdef SUPPORT_OPENGL
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(GL_GREATER, ALPHA_TEST_TRESHOLD_F);
#   endif
		}

#	ifdef SUPPORT_OPENGL
		if (GLEW_ARB_multitexture)
#	endif
			glActiveTexture(GL_TEXTURE0);

#	ifdef SUPPORT_OPENGL
		// Obsolete but in case.
		glDisable(GL_LIGHTING);
		glColor4f(1.f, 1.f, 1.f, 1.f);

		glEnable(GL_TEXTURE_2D);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(g_ProjectionMatrix);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(g_WorldMatrix);
#	elif SUPPORT_OPENGLES
		const GLint uidx = g_DoBlending ? 0 : 1;
		glUseProgram(g_CurProgram);
		glUniform1i(g_TextureUIdx[uidx], 0);
		glUniformMatrix4fv(g_WorldMatrixUIdx[uidx], 1, GL_FALSE, g_WorldMatrix);
		glUniformMatrix4fv(g_ProjMatrixUIdx[uidx], 1, GL_FALSE, g_ProjectionMatrix);
#	endif

		glBindTexture(GL_TEXTURE_2D, (GLuint)(size_t)(g_TexturePointer));

#ifdef SUPPORT_OPENGL
		if (!GLEW_ARB_vertex_buffer_object) {
#endif
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

			if (g_Vbo != -1)
				glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
			else
				glBindBuffer(GL_ARRAY_BUFFER, 0);

#	ifdef SUPPORT_OPENGL
		}

		glDisableClientState(GL_COLOR_ARRAY);

#	ifdef SUPPORT_OPENGL
		if (GLEW_ARB_multitexture)
#	endif
			glClientActiveTexture(GL_TEXTURE0);

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), g_Vbo != -1 ? (GLvoid *)(3 * sizeof(float)) : &g_Data[0].s);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), g_Vbo != -1 ? 0 : &g_Data[0].x);
#	elif SUPPORT_OPENGLES
			glEnableVertexAttribArray(SHADER_ATTRIB_POS);
			glVertexAttribPointer(SHADER_ATTRIB_POS, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), g_Vbo != -1 ? 0 : g_Data);

			glEnableVertexAttribArray(SHADER_ATTRIB_UV);
			glVertexAttribPointer(SHADER_ATTRIB_UV, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), g_Vbo != -1 ? (GLvoid *)(3 * sizeof(float)) : &g_Data[0].s);
#	endif

			glDrawArrays(GL_TRIANGLES, first * 6, count * 6);
	}
		break;
#endif
	}
}